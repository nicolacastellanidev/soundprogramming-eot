# SOUND PROGRAMMING - EOT PROJECT

Creato da ```Nicola Castellani (VR361872)``` per il corso di Sound Programming (MGD2021).

## STRUTTURA DEL PROGETTO

Ho deciso di creare una **loop machine** mescolata ad una **drum machine**:  

1. ***Loop Machine***: ci sono 3 loop che si possono mescolare insieme, una volta partito il primo gli altri si accodano, non partono immediatamente ma attendono la fine dei loop attuali.

2. ***Drum Machine***: ci sono 2 sounds one shot che possono essere suonati immediatamente.

Il progetto si compone di 2 sottoprogetti.

1. ```FMODWrapper```  
   Libreria statica che fa da wrapper custom con le librerie FMOD.
2. ```SoundEOTProject```  
   Progetto eseguibile che espone l'interfaccia grafica per interagire con l'FMODWrapper.

## HOT TO PLAY

E' possibile aprire la solution con VS e lanciare l'app direttamente dall'IDE, oppure mediante il file ***Debug/SoundEOTProject.exe*** all'interno della cartella del progetto.

## DEFINIZIONE OBBIETTIVI E RISOLUZIONE

### ```Init System```

La funzione ***FMODWrapper***::```FMODInit``` si preoccupa di inizializzare il sistema FMOD.

### ```Load (static e streaming)```

La funzione ***FMODWrapper***::```LoadSound``` carica i suoni in modo statico mentre la funzione ***FMODWrapper***::```LoadSoundStreaming``` carica i souni in modalità stream.

### ```Play, Pause```

Per questo punto ho deciso di gestire il play in loop (PlaySound) e one shot (PlaySoundOneShot), la pausa avviene sul gruppo master in modo che tutti i suoni vadano in pausa.

I metodi  ***FMODWrapper***::```PauseAll``` e  ***FMODWrapper***::```ResumeAll``` permettono di mettere in pausa e fare il resume dei suoni.

### ```Stop```

Lo stop viene gestito in modo particolare a seconda che venga utilizzato mediante PlaySound o PlaySoundOneShot (in generale un suono si stoppa se l'utente preme la action di play su un suono che sta già suonando, il play è quindi un toggle sullo stato del suono):

1. ```PlaySound```

    * se il suono prevede di essere inserito nella loop machine allora:
        * se era nella **loopQueue** allora semplicemente non verrà fatto partire
        * altrimenti viene messo in una **stopsQueue** in modo che venga fermato quando finisce il suo loop
    * altrimenti se il canale sta suonando viene fermato

2. ```PlaySoundOneShot```
    * Qui il concetto di stop non esiste essendo un play one shot.

### ```Pan (Left/Right)```

Ho settato un pan di default per i canali relativi al loop atmos e drums, sul loro canale. L'utente ha la possibilità di settare comunque un pan (sull'asse X e Y) sul canale master mediante due action dedicate.  

Il metodo ***FMODWrapper***::```UpdatePan``` si preoccupa di gestire il pan del canale master.

### ```Volume```

Il metodo ***FMODWrapper***::```UpdateVolume``` si occupa di gestire il volume sul canale master, è comunque possibile settare il volume sui singoli canali mediante i metodi ```PlaySound``` e ```PlaySoundOneShot```.

### ```Canali Audio```
Ho implementato una serie di canali audio:

* channel1
* channel2
* channel3
* instrumentChannel
* musicChannel

ed una serie di Channel group:

* loopGroup
* instrumentGroup
* musicGroup
* masterGroup

In genere i primi 3 canali vengono usati per la loop machine ed appartengono al **loopGroup**, mentre i suoni one shot fanno parte dell'instrumentChannel, raggruppato all'interno dell'omonimo gruppo **instrumentGroup**.

Il gruppo **musicGroup** si occupa di gestire il canale musicChannel mentre il **masterGroup** gestire tutto ciò che riguarda il canale master.