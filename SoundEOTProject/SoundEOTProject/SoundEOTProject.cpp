// SoundEOTProject.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <fmod.hpp>
#include <conio.h>
#include <chrono>
#include <thread>

#include "../FMODWrapper/FMODWrapper.h"
#include "SoundEOTProject.h"
	
void ClearConsole()
{
#if defined _WIN32
	system("cls");
	//clrscr(); // including header file : conio.h
#elif defined (__LINUX__) || defined(__gnu_linux__) || defined(__linux__)
	system("clear");
	//std::cout<< u8"\033[2J\033[1;1H"; //Using ANSI Escape Sequences 
#elif defined (__APPLE__)
	system("clear");
#endif
}

static unsigned int gPressedButtons = 0;			// stores pressed buttons
static unsigned int gDownButtons = 0;				// stores down buttons
static bool			gPaused = false;				// check if sounds are paused
const int			INTERFACE_UPDATETIME = 10;		// 50ms update for interface
static float		actualPanHorizontal = 0;		// stores actual pan on X axis
static float		actualPanVertical = 0;			// stores actual pan on Y axis
static float		actualVolume = 1.f;				// stores actual volume

// Enum to action
enum Common_Button
{
	BTN_ACTION1,
	BTN_ACTION2,
	BTN_ACTION3,
	BTN_ACTION4,
	BTN_ACTION5,
	BTN_LEFT,
	BTN_RIGHT,
	BTN_UP,
	BTN_DOWN,
	BTN_QUIT,
	BTN_PLUS,
	BTN_MINUS,
	BTN_V,
	BTN_N
};

// bitwise check on button pressed
bool Common_BtnPress(Common_Button btn)
{
	return ((gPressedButtons & (1 << btn)) != 0);
}

// main custom wrapper
NicolaCastellani::FMODWrapper* wrapper;
// list of loaded sounds
FMOD::Sound* atmos = NULL, * drums = NULL, * guitar = NULL,
*kickSound = NULL, *snareSound = NULL, *musicSound = NULL;
// list of channel group
FMOD::ChannelGroup* loopGroup, * instrumentGroup, * musicGroup, * masterGroup;
// list of channels
FMOD::Channel* channel1 = 0, * channel2 = 0, * channel3 = 0, *instrumentChannel = 0, *musicChannel = 0;

/// <summary>
/// Updates the wrapper FMOD system
/// </summary>
void Update() {
	wrapper->Update();
}

/// <summary>
/// Render to console
/// </summary>
/// <returns></returns>
void Render() {
	ClearConsole();
	
	printf("------ LOOP MACHINE (Static loaded loops) ------\n");
	
	printf("[1] - Play/Stop Atmos ");
	printf("[2] - Play/Stop Drums ");
	printf("[3] - Play/Stop Guitar\n");
	
	printf("------ DRUM MACHINE (Static loaded one shots) ------\n");
	
	printf("[V] - Play Kick ");
	printf("[N] - Play Snare\n");

	printf("------ MUSIC (Stream loaded music) ------\n");
	printf("[4] - Play/Stop cool music\n");
	
	printf("------ ACTIONS ------\n");

	printf("[5]   - Pause all channels\n");
	printf("[LEFT] - PAN left ");
	printf("[RIGHT] - PAN right\n");
	printf("[UP] - PAN forward ");
	printf("[DOWN] - PAN backward\n");
	printf("[+] - Volume up ");
	printf("[-] - Volume down\n");
	printf("[ESC] - Exit\n");
	
	wrapper->Render();
}

// sleep thread
void Sleep() {
	std::this_thread::sleep_for(std::chrono::milliseconds(INTERFACE_UPDATETIME - 1));
}

// handles button click
void ProcessInput() {
	unsigned int newButtons = 0;
	while (_kbhit())
	{
		wint_t key = _getwch();
		if (key == 0 || key == 224)
		{
			key = 256 + _getwch(); // Handle multi-char keys
		}

		if (key == '1')				newButtons |= (1 << BTN_ACTION1);
		else if (key == '2')		newButtons |= (1 << BTN_ACTION2);
		else if (key == '3')		newButtons |= (1 << BTN_ACTION3);
		else if (key == '4')		newButtons |= (1 << BTN_ACTION4);
		else if (key == '5')		newButtons |= (1 << BTN_ACTION5);
		else if (key == 256 + 75)	newButtons |= (1 << BTN_LEFT);
		else if (key == 256 + 77)	newButtons |= (1 << BTN_RIGHT);
		else if (key == 256 + 72)	newButtons |= (1 << BTN_UP);
		else if (key == 256 + 80)	newButtons |= (1 << BTN_DOWN);
		else if (key == 43)			newButtons |= (1 << BTN_PLUS);
		else if (key == 45)			newButtons |= (1 << BTN_MINUS);
		else if (key == 118)		newButtons |= (1 << BTN_V);
		else if (key == 110)		newButtons |= (1 << BTN_N);
		else if (key == 27)			newButtons |= (1 << BTN_QUIT);
	}

	gPressedButtons = (gDownButtons ^ newButtons) & newButtons;
	gDownButtons = newButtons;

	if (Common_BtnPress(BTN_ACTION1))	wrapper->PlaySound(&atmos,loopGroup, false, &channel1, 1.f);
	if (Common_BtnPress(BTN_ACTION2))	wrapper->PlaySound(&drums,loopGroup, false, &channel2, -1.f);
	if (Common_BtnPress(BTN_ACTION3))	wrapper->PlaySound(&guitar,loopGroup, false, &channel3, 0.f);
	if (Common_BtnPress(BTN_ACTION4))	wrapper->PlaySound(&musicSound, musicGroup, false, &musicChannel, 0.f, 1.f, false);
	if (Common_BtnPress(BTN_V))			wrapper->PlaySoundOneShot(&kickSound, instrumentGroup,	&instrumentChannel, 0.f, 1.f);
	if (Common_BtnPress(BTN_N))			wrapper->PlaySoundOneShot(&snareSound, instrumentGroup, &instrumentChannel, 0.f, 1.f);
	if (Common_BtnPress(BTN_ACTION5)) {
		if(gPaused)						wrapper->ResumeAll(&masterGroup);
		else							wrapper->PauseAll(&masterGroup);
		gPaused = !gPaused;
	}
	if (Common_BtnPress(BTN_LEFT)) {
		actualPanHorizontal -= 1.f;
		wrapper->UpdatePan(&masterGroup, actualPanHorizontal, actualPanVertical);
	}
	if (Common_BtnPress(BTN_RIGHT)) {
		actualPanHorizontal += 1.f;
		wrapper->UpdatePan(&masterGroup, actualPanHorizontal, actualPanVertical);
	}

	if (Common_BtnPress(BTN_UP)) {
		actualPanVertical += 1.f;
		wrapper->UpdatePan(&masterGroup, actualPanHorizontal, actualPanVertical);
	}
	if (Common_BtnPress(BTN_DOWN)) {
		actualPanVertical -= 1.f;
		wrapper->UpdatePan(&masterGroup, actualPanHorizontal, actualPanVertical);
	}

	if (Common_BtnPress(BTN_PLUS)) {
		actualVolume += 0.1f;
		if (actualVolume > 1.f) actualVolume = 1.f;
		wrapper->UpdateVolume(&masterGroup, actualVolume);
	}

	if (Common_BtnPress(BTN_MINUS)) {
		actualVolume -= 0.1f;
		if (actualVolume < 0.f) actualVolume = 0.f;
		wrapper->UpdateVolume(&masterGroup, actualVolume);
	}
}

int main()
{
	// create the wrapper
	wrapper = new NicolaCastellani::FMODWrapper();

	// init fmod
	if (!wrapper->FMODInit()) {
		printf("[!] FMOD NOT INITIALIZED\n");
		exit(-1);
    }
    else {
        printf(">>> FMOD INITIALIZED\n");
		// load sounds
		atmos = wrapper->LoadSound("./media/atmos.wav", FMOD_LOOP_NORMAL);
		if (atmos == nullptr) {
			printf("[!] CANNOT LOAD SOUND\n");
			exit(-1);
		}
        else printf("- SOUND 1 LOADED CORRECTLY\n");

		drums = wrapper->LoadSound("./media/drums.wav", FMOD_LOOP_NORMAL);
		if (drums == nullptr) {
			printf("[!] CANNOT LOAD SOUND\n");
			exit(-1);
		}
		else printf("- SOUND 2 LOADED CORRECTLY\n");

		guitar = wrapper->LoadSound("./media/melody.wav", FMOD_LOOP_NORMAL);
		if (guitar == nullptr) {
			printf("[!] CANNOT LOAD SOUND\n");
			exit(-1);
		}
		else printf("- SOUND 3 LOADED CORRECTLY\n");

		kickSound = wrapper->LoadSound("./media/kick.wav", FMOD_LOOP_OFF);
		if (kickSound == nullptr) {
			printf("[!] CANNOT LOAD SOUND\n");
			exit(-1);
		}
		else printf("- KICK LOADED CORRECTLY\n");

		snareSound = wrapper->LoadSound("./media/snare.wav", FMOD_LOOP_OFF);
		if (kickSound == nullptr) {
			printf("[!] CANNOT LOAD SOUND\n");
			exit(-1);
		}
		else printf("- SNARE LOADED CORRECTLY\n");

		musicSound = wrapper->LoadSoundStreaming("./media/music.mp3", FMOD_LOOP_NORMAL);
		if (musicSound == nullptr) {
			printf("[!] CANNOT LOAD SOUND\n");
			exit(-1);
		}
		else printf("- MUSIC LOADED CORRECTLY\n");

		// create channel groups
		wrapper->GetSystem()->createChannelGroup("Loop Group", &loopGroup);
		wrapper->GetSystem()->createChannelGroup("Instruments group", &instrumentGroup);
		wrapper->GetSystem()->createChannelGroup("Music group", &musicGroup);
		wrapper->GetSystem()->getMasterChannelGroup(&masterGroup);

		// add them to master
		masterGroup->addGroup(loopGroup);
		masterGroup->addGroup(instrumentGroup);
		masterGroup->addGroup(musicGroup);
		masterGroup->setMode(FMOD_3D);
    }

	// game update loop
    while (!Common_BtnPress(BTN_QUIT))
    {
		ProcessInput();
		Update();
		Render();
		Sleep();
    }
	// release all when program ends
	wrapper->Release({ atmos, drums, guitar }, {0});
}