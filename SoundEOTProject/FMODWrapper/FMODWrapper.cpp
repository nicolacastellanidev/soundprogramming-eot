﻿// FMODWrapper.cpp : Defines the functions for the static library.
//

#include "pch.h"
#include "framework.h"
#include "FMODWrapper.h"
#include <stdio.h>
#include "fmod_errors.h"
#include <string>

const float DISTANCEFACTOR = 1.0f;          // Units per meter.  I.e feet would = 3.28.  centimeters would = 100.
const char* NICA_LOOP_END = "LOOP_END";
std::string LOADER_PARTS = "|/-\\|/-";
int currentLoaderIndex = 0;

std::vector<FMOD::Channel*> loopQueue;
std::vector<FMOD::Channel*> playingQueue;
std::vector<FMOD::Channel*> stopsQueue;

FMOD_RESULT F_CALLBACK NicolaCastellani::FMODWrapper::LoopEndedCallback(
	FMOD_CHANNELCONTROL* chanControl, 
	FMOD_CHANNELCONTROL_TYPE controlType,
	FMOD_CHANNELCONTROL_CALLBACK_TYPE callbackType,
	void* commandData1,
	void* commandData2)
{
	auto _channel = (FMOD::Channel*)chanControl;
	if(callbackType == FMOD_CHANNELCONTROL_CALLBACK_SYNCPOINT) { // called after loop ended
		if (loopQueue.size() > 0) { // play everything in the loop queue
			for (FMOD::Channel* channel : loopQueue) {
				if (channel) {
					channel->setPaused(false);
					NicolaCastellani::FMODWrapper::AddTo(&playingQueue, channel);
				}
			}
			// clear the loop queue
			loopQueue.clear();
		}

		if (std::find(stopsQueue.begin(), stopsQueue.end(), _channel) != stopsQueue.end()) {
			// sound should be stopped
			_channel->stop();
			NicolaCastellani::FMODWrapper::RemoveFrom(&stopsQueue, _channel);
			NicolaCastellani::FMODWrapper::RemoveFrom(&playingQueue, _channel);
		}
	}
	else if (callbackType == FMOD_CHANNELCONTROL_CALLBACK_END) { // sound ended (not called if sound is looping)
		NicolaCastellani::FMODWrapper::RemoveFrom(&playingQueue, _channel);
	}
	return FMOD_OK;
}

NicolaCastellani::FMODWrapper::FMODWrapper()
{
	printf("FMODWrapper created\n");
}

bool NicolaCastellani::FMODWrapper::FMODInit()
{
	FMOD_RESULT result;

	result = FMOD::System_Create(&system); // Create the Studio System object.
	if (!CheckResult(result)) { return false; }

	// Initialize FMOD Studio, which will also initialize FMOD Core
	result = system->init(24, FMOD_INIT_NORMAL, 0);
	if (!CheckResult(result)) { return false; }

	
	// Set the distance units. (meters/feet etc).
	result = system->set3DSettings(1.0, DISTANCEFACTOR, 1.0f);
	if (!CheckResult(result)) { return false; }

	return true;
}

void NicolaCastellani::FMODWrapper::Update()
{
	system->update();
}

FMOD::Sound* NicolaCastellani::FMODWrapper::LoadSound(const char* filePath, FMOD_MODE mode) {
	FMOD_RESULT result;
	FMOD::Sound* loadedSound;

	result = system->createSound(filePath, FMOD_3D, 0, &loadedSound);
	if (!CheckResult(result)) { return nullptr; }

	result = loadedSound->setMode(mode);
	if (!CheckResult(result)) { return nullptr; }

	return loadedSound;
}

FMOD::Sound* NicolaCastellani::FMODWrapper::LoadSoundStreaming(const char* filePath, FMOD_MODE mode) {
	FMOD_RESULT result;
	FMOD::Sound* loadedSound;

	result = system->createStream(filePath, FMOD_3D, 0, &loadedSound);
	if (!CheckResult(result)) { return nullptr; }

	result = loadedSound->setMode(mode);
	if (!CheckResult(result)) { return nullptr; }

	return loadedSound;
}

bool NicolaCastellani::FMODWrapper::PlaySound(
	FMOD::Sound** sound,
	FMOD::ChannelGroup* channelgroup,
	bool paused,
	FMOD::Channel** channel,
	float pan,
	float volume,
	bool waitForNextLoop
) {
	FMOD_RESULT result;
	FMOD_VECTOR pos = { pan * DISTANCEFACTOR, 0.0f, 0.0f };
	FMOD_VECTOR vel = { 0.0f, 0.0f, 0.0f };

	bool playing = 0;
	(*channel)->isPlaying(&playing);
	(*channel)->setChannelGroup(channelgroup);


	if (!waitForNextLoop) {
		if (playing) {
			(*channel)->stop();
			return true;
		}
		else {
			result = system->playSound(*sound, 0, true, &(*channel));
			if (!CheckResult(result)) { return false; }
			(*channel)->setPaused(false);
			return true;
		}
	}

	if (playing) {
		if (std::find(loopQueue.begin(), loopQueue.end(), (*channel)) != loopQueue.end()) {
			// if we are waiting for loop just remove channel from that
			NicolaCastellani::FMODWrapper::RemoveFrom(&loopQueue, *channel);
			(*channel)->stop();
		}
		else {
			// add to stopQueue
			NicolaCastellani::FMODWrapper::AddTo(&stopsQueue, *channel);
		}
		return true;
	}

	result = system->playSound(*sound, 0, true, &(*channel));
	if (!CheckResult(result)) { return false; }

	(*channel)->setVolume(volume);

	result = (*channel)->set3DAttributes(&pos, &vel);
	if (!CheckResult(result)) { return false; }

	if (playingQueue.size() == 0) {
		result = (*channel)->setPaused(false);
		NicolaCastellani::FMODWrapper::AddTo(&playingQueue, (*channel));
		if (!CheckResult(result)) { return false; }
	}
	else {
		NicolaCastellani::FMODWrapper::AddTo(&loopQueue, (*channel));
	}

	unsigned int length;
	FMOD_SYNCPOINT* ptr;

	result = (*sound)->getLength(&length, FMOD_TIMEUNIT_MS);
	if (!CheckResult(result)) { return false; }

	result = (*sound)->addSyncPoint(length, FMOD_TIMEUNIT_MS, NICA_LOOP_END, &ptr);
	if (!CheckResult(result)) { return false; }

	result = (*channel)->setCallback(&NicolaCastellani::FMODWrapper::LoopEndedCallback);
	if (!CheckResult(result)) { return false; }

	return true;
}

bool NicolaCastellani::FMODWrapper::PlaySoundOneShot(
	FMOD::Sound** sound,
	FMOD::ChannelGroup* channelgroup,
	FMOD::Channel** channel,
	float pan,
	float volume
) {
	FMOD_RESULT result;
	FMOD_VECTOR pos = { pan * DISTANCEFACTOR, 0.0f, 0.0f };
	FMOD_VECTOR vel = { 0.0f, 0.0f, 0.0f };

	result = system->playSound(*sound, 0, true, &(*channel));
	if (!CheckResult(result)) { return false; }

	(*channel)->setPaused(false);
	(*channel)->setVolume(volume);

	result = (*channel)->set3DAttributes(&pos, &vel);
	if (!CheckResult(result)) { return false; }

	return true;
}

bool NicolaCastellani::FMODWrapper::CheckResult(FMOD_RESULT result) {
	if (result != FMOD_OK)
	{
		printf(FMOD_ErrorString(result));
		printf("\n");
		return false;
	}
	return true;
}

void NicolaCastellani::FMODWrapper::Release(
	std::vector<FMOD::Sound*> sounds, 
	std::vector<FMOD::ChannelGroup*> channelGroups) {
	for (auto sound: sounds)
	{
		sound->release();
	}
	for (auto group : channelGroups)
	{
		group->release();
	}
	system->release();
}

void NicolaCastellani::FMODWrapper::RemoveFrom(std::vector<FMOD::Channel*>* channels, FMOD::Channel* channelToDelete) {
	int index = 0;
	for (FMOD::Channel* channel: *channels)
	{
		if (channel == channelToDelete)
		{
			channels->erase(channels->begin() + index);
		}
		++index;
	}
}

void NicolaCastellani::FMODWrapper::AddTo(std::vector<FMOD::Channel*>* channels, FMOD::Channel* channelToAdd) {
	int index = 0;
	for (FMOD::Channel* channel : *channels)
	{
		if (channel == channelToAdd)
		{
			return; // skip insert if element exists
		}
		++index;
	}
	channels->push_back(channelToAdd);
}

bool NicolaCastellani::FMODWrapper::PauseAll(FMOD::ChannelGroup** master) {
	(*master)->setPaused(true);
	return true;
}

bool NicolaCastellani::FMODWrapper::ResumeAll(FMOD::ChannelGroup** master) {
	(*master)->setPaused(false);
	return true;
}


bool NicolaCastellani::FMODWrapper::UpdatePan(
	FMOD::ChannelGroup** master,
	float hPan,
	float vPan
) {
	FMOD_VECTOR pos = { hPan * DISTANCEFACTOR, vPan * DISTANCEFACTOR, 0.0f };
	FMOD_VECTOR vel = { 0.0f, 0.0f, 0.0f };

	auto result = (*master)->set3DAttributes(&pos, &vel);
	if (CheckResult(result)) { return false; }
	return true;
}

bool NicolaCastellani::FMODWrapper::UpdateVolume(FMOD::ChannelGroup** master, float volume) {
	auto result = (*master)->setVolume(volume);
	if (CheckResult(result)) { return false; }
	return true;
}

void NicolaCastellani::FMODWrapper::Render() {

	printf("\n\n°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°   NOW PLAYING   °°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°\n\n");
	for (FMOD::Channel* channel : playingQueue) {

		PrintInfoForChannel(channel, 'P');
	}

	printf("\n\n°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°    IN QUEUE     °°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°\n\n");
	for (FMOD::Channel* channel : loopQueue) {

		PrintInfoForChannel(channel, LOADER_PARTS.at(currentLoaderIndex));
	}

	printf("\n\n°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°    STOPPING     °°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°\n\n");
	for (FMOD::Channel* channel : stopsQueue) {

		PrintInfoForChannel(channel, LOADER_PARTS.at(currentLoaderIndex));
	}

	if(++currentLoaderIndex >= 7) {
		currentLoaderIndex = 0;
	}
}

void NicolaCastellani::FMODWrapper::PrintInfoForChannel(FMOD::Channel* channel, char prefix) {
	FMOD::DSP* ChanDSP = nullptr;
	channel->getDSP(FMOD_CHANNELCONTROL_DSP_HEAD, &ChanDSP);

	FMOD::Sound* Sound;
	channel->getCurrentSound(&Sound);

	char* SoundName = 0;
	auto result = Sound->getName(SoundName, 256);

	if (ChanDSP)
	{
		ChanDSP->setMeteringEnabled(false, true);
		FMOD_DSP_METERING_INFO Info = {};
		ChanDSP->getMeteringInfo(nullptr, &Info);
		printf("[%c] --- Metering: %d channels, %d len.\n        rms: %.3f, %.3f, %.3f, %.3f, %.3f, %.3f\n",
			prefix, Info.numchannels, Info.numsamples,
			Info.rmslevel[0], Info.rmslevel[1], Info.rmslevel[2], Info.rmslevel[3], Info.rmslevel[4], Info.rmslevel[5]);
	}
}