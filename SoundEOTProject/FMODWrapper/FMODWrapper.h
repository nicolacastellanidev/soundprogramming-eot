#pragma once
#include <fmod.hpp>
#include <vector>

namespace NicolaCastellani {
	class FMODWrapper {
	private:
		// main FMOD system
		FMOD::System* system = 0;
		// utility method to check if fmod operation is success
		bool CheckResult(FMOD_RESULT result);
		// utilities for vector management
		static void RemoveFrom(std::vector<FMOD::Channel*>* channels, FMOD::Channel* channelToDelete);
		static void AddTo(std::vector<FMOD::Channel*>* channels, FMOD::Channel* channelToAdd);
		// prints info
		static void PrintInfoForChannel(FMOD::Channel* channel, char prefix);

	public:
		// default constructor - used for logging
		FMODWrapper();
		// update lifecycle method
		void Update();
		// initialize FMOD
		bool FMODInit();

		// getter for wrapper system
		FMOD::System* GetSystem() { return system; }

		// load a sound from filepath
		FMOD::Sound* LoadSound(
			const char* filePath, 
			FMOD_MODE mode
		);

		// same as LoadSound, but by streaming
		FMOD::Sound* LoadSoundStreaming(
			const char* filePath,
			FMOD_MODE mode
		);

		// pauses master channel group
		bool PauseAll(
			FMOD::ChannelGroup** master
		);

		// resumes master channel group
		bool ResumeAll(
			FMOD::ChannelGroup** master
		);

		// Updates master channel group volume [0..1]
		bool UpdateVolume(
			FMOD::ChannelGroup** master,
			float volume
		);

		// updates master channel pan
		bool UpdatePan(
			FMOD::ChannelGroup** master,
			float hPan,
			float vPan
		);

		// Play a sound in a drum loop machine
		// waiting for current loop to complete before playing current
		bool PlaySound(
			// the sound to play
			FMOD::Sound** sound,
			// the group to assing
			FMOD::ChannelGroup* channelgroup,
			// if paused or not
			bool paused,
			// the channel to play
			FMOD::Channel** channel,
			// channel pan
			float pan = 0.f,
			// channel volume
			float volume = 1.f,
			// wait or not
			bool waitForNextLoop = true
		);

		// play a sound one shot: don't wait for next loop, play immediately
		bool PlaySoundOneShot(
			// the sound to play
			FMOD::Sound** sound,
			// the group to assign
			FMOD::ChannelGroup* channelgroup,
			// channel to play
			FMOD::Channel** channel,
			// channel volume
			float pan = 0.f,
			// channel pan
			float volume = 1.f
		);

		// releases all sounds and channel groups
		void Release(std::vector<FMOD::Sound*> sounds, std::vector<FMOD::ChannelGroup*> channelGroups);

		// callback on channel
		static FMOD_RESULT F_CALLBACK LoopEndedCallback(
			FMOD_CHANNELCONTROL* chanControl,
			FMOD_CHANNELCONTROL_TYPE controlType,
			FMOD_CHANNELCONTROL_CALLBACK_TYPE callbackType,
			void* commandData1,
			void* commandData2
		);

		// render method, called in a game loop
		void Render();
	};
}